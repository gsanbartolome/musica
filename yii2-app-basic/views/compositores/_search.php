<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\db $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="compositores-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NOMBRE_COMPLETO') ?>

    <?= $form->field($model, 'FOTOGRAFIA') ?>

    <?= $form->field($model, 'BIOGRAFIA') ?>

    <?= $form->field($model, 'FECHA_NACIMIENTO') ?>

    <?php // echo $form->field($model, 'FECHA_MUERTE') ?>

    <?php // echo $form->field($model, 'INSTRUMENTO') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
