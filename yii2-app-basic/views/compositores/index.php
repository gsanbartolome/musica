<?php

use app\models\Compositores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var app\models\db $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Compositores';
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
.custom-table {
    border: 4px solid black;
    border-radius: 10px;
}

.custom-table th,
.custom-table td {
    border: 2px solid black;
}

.center {
  display: flex;
  justify-content: center;
  align-items: center;
}

.line-separator {
    width: 100%;
    height: 2px;
    background-color: black;
    margin: 20px 0;
}

.attribute-text {
    font-family: "Times New Roman", serif;
    font-weight: bold;
    font-size: 20px;
    text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
}

.image-container {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    box-shadow: 0 0 5px rgba(0,0,0,0.5);
    border: 4px solid black;
    overflow: hidden;
}

.image-container img {
    width: 100%;
    height: 100%;
    object-fit: cover;
}
</style>

<div class="compositores-index">

    <h1 style="text-align: center; font-family: Algerian; font-size: 60px;"><?= Html::encode($this->title) ?></h1>

    <div style="text-align: center; font-family: Garamond; font-size: 20px; margin-top: 50px;">
        En esta sección podrás acceder al gran catálogo de compositores de los que disponemos, puedes entrar en cada uno de ellos para más información o buscar a alguno en específico usando nuestra barra de búsqueda.
    </div>

    <p>
        <?= Html::a('Create Compositores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <?= $form->field($searchModel, 'NOMBRE_COMPLETO')->textInput(['maxlength' => true, 'placeholder' => 'Buscar por nombre'])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <?= $form->field($searchModel, 'INSTRUMENTO')->textInput(['maxlength' => true, 'placeholder' => 'Buscar por instrumento'])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="table-responsive" style="min-width: fit-content;">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                $content = '<div class="row">';
                $content .= '<div class="col-lg-4 center">';
                if ($model->FOTOGRAFIA) {
                    $content .= '<div class="image-container">';
                    $content .= '<img src="' . Yii::getAlias('@web') . '/' . $model->FOTOGRAFIA . '" alt="Imagen">';
                    $content .= '</div>';
                }
                $content .= '</div>';

                $content .= '<div class="col-lg-4 center">';
                $content .= '<span class="attribute-text">' . Html::encode($model->NOMBRE_COMPLETO) . '</span>';
                $content .= '</div>';

                $content .= '<div class="col-lg-4 center">';
                $content .= '<span class="attribute-text">' . Html::encode($model->INSTRUMENTO) . '</span>';
                $content .= '</div>';

                $content .= '</div>';

                $content .= '<div class="row">';
                $content .= '<div class="col-lg-4 center">';
                $content .= Html::a('Ver detalles', ['view', 'ID' => $model->ID], ['class' => 'btn btn-primary']);
                $content .= '</div>';
                $content .= '</div>';

                $content .= '<div class="line-separator"></div>';

                return $content;
            },
        ]) ?>
    </div>
</div>
