<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Compositores $model */
/** @var yii\widgets\ActiveForm $form */


?>
<style>
    .center-input {
  text-align: center;
}
</style>


<div class="compositores-form">

    <?php $form = ActiveForm::begin(); ?>
    

   <?= $form->field($model, 'NOMBRE_COMPLETO')->textInput(['maxlength' => true, 'class' => 'center-input']) ?>
    
    <div style="background-image: url('<?= Yii::getAlias('@web') . '/' . $model->FOTOGRAFIA ?>'); width: 300px; height: 300px; background-size: cover;"></div>

    <?= $form->field($model, 'FOTO')->fileInput() ?>

    <?= $form->field($model, 'BIOGRAFIA')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'FECHA_NACIMIENTO')->textInput() ?>

    <?= $form->field($model, 'FECHA_MUERTE')->textInput() ?>

    <?= $form->field($model, 'INSTRUMENTO')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
