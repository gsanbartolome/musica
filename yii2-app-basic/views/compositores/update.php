<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Compositores $model */

$this->title = 'Update Compositores: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Compositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'ID' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="compositores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
