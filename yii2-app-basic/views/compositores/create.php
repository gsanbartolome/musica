<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Compositores $model */

$this->title = 'Create Compositores';
$this->params['breadcrumbs'][] = ['label' => 'Compositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compositores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
