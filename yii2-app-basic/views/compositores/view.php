<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Compositores $model */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Compositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<style>
    .custom-container {
        background-color: #D2B48C;
        border-radius: 20px;
        padding: 20px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
        position: relative;
    }

    .custom-container::before {
        content: "";
        position: absolute;
        top: -10px;
        left: -10px;
        right: -10px;
        bottom: -10px;
        border-radius: 20px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
        z-index: -1;
    }

    .custom-row {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 10px;
    }

    .custom-label {
        font-weight: bold;
        margin-right: 10px;
    }

    .custom-value {
        text-align: center;
    }

    .custom-image {
        display: flex;
        justify-content: center;
    }

    .custom-biography {
        text-align: justify;
    }
</style>

<div class="compositores-view" >
    <h1 style="font-size: 36px; font-family: 'Times New Roman', Times, serif; text-decoration: underline; text-align: center;">
    <?= strtoupper(Html::encode($model->NOMBRE_COMPLETO)) ?>
</h1>

    <div class="compositores-view" style="margin-top: 40px;">
    <?= DetailView::widget([
        'model' => $model,
        'template' => '
            <div class="custom-container">
                <div class="custom-row">
                    <div class="custom-label">{label}</div>
                </div>
                <div class="custom-row">
                    <div class="custom-value">{value}</div>
                </div>
            </div>
        ',
        'attributes' => [
            [
    'attribute' => 'FOTOGRAFIA',
    'value' => function ($model) {
        $image = Html::img(Yii::getAlias('@web') . '/' . $model->FOTOGRAFIA, [
            'style' => 'width: 400px; height: 400px; object-fit: cover; border-radius: 50%; border: 5px solid black; position: relative; z-index: 2;',
        ]);
        $watermark = Html::img('https://www.viniloscasa.com/17258-thickbox/vinilo-decorativo-adhesivo-pegatina-filigrana.jpg', [
            'style' => 'position: absolute; top: 0; left: 0; opacity: 0.3; width: 100%; height: 100%; z-index: 1;',
        ]);
        return $watermark . $image;
    },
    'format' => 'raw',
    'label' => '',
    'valueColOptions' => ['class' => 'custom-image'],
    'labelColOptions' => ['style' => 'display: none'],
],
            [
                'attribute' => 'BIOGRAFIA',
                'format' => 'ntext',
                'valueColOptions' => ['class' => 'custom-biography'],
                'labelColOptions' => ['style' => 'display: none'],
            ],
        ],
    ]) ?>
</div>

