<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    
    
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style>
        
     .navbar {
    background: linear-gradient(to bottom, #ffffff, #d3b38c); /* Degradado vertical de blanco a marrón claro */
    border-radius: 10px; /* Puntas redondeadas */
}
        
        .navbar-nav .nav-link {
            font-family: "Times New Roman", Times, serif; /* Tipografía más clásica */
            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5); /* Sombreado del texto */
        }
    </style>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-light bg-gradient-light',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Inicio', 'url' => ['/site/index'], 'options' => ['class' => 'nav-item']],
            ['label' => 'Compositores', 'url' => ['/compositores/index'], 'options' => ['class' => 'nav-item']],
            ['label' => 'Partituras', 'url' => ['/composiciones/index'], 'options' => ['class' => 'nav-item']],
            ['label' => 'Sobre Nosotros', 'url' => ['/site/about'], 'options' => ['class' => 'nav-item']],
            ['label' => 'Contacto', 'url' => ['/site/contact'], 'options' => ['class' => 'nav-item']],
            //Yii::$app->user->isGuest ? (
                //['label' => 'Login', 'url' => ['/site/login'], 'options' => ['class' => 'nav-item']]
            //) : (
                //'<li class="navbar-text">'
               // . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                //. Html::submitButton(
                 //   'Logout (' . Yii::$app->user->identity->username . ')',
                  //  ['class' => 'btn btn-link logout']
                //)
               // . Html::endForm()
               // . '</li>'
           // )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>