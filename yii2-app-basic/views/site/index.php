<?php
/** @var yii\web\View $this */
$this->title = 'My Yii Application';
?>
<style>
    body {
        margin: 0;
        padding: 0;
        background-color: #f5deb3; /* color marrón claro */
    }
    
    .custom-image-container {
        margin-left: 60px; /* Ajusta el valor del margen según sea necesario */
    }
    
    .jumbotron {
        background-image: url("https://kubomusical.com/wp-content/uploads/2020/04/old-paper-vintage-sheet-music-30973371.jpg");
        background-size: cover;
        background-position: center;
        height: 550px; /* Ajusta la altura según sea necesario */
        animation: fadeIn 1s ease-in-out;
    }
    
    .btn-outline-secondary:hover {
        transform: scale(0.95);
        background-color: #87CEFA; /* Azul claro */
        color: #000; /* Color de texto */
    }
    
    .custom-hover {
        background-color: #8AE29B !important; /* Cambia el color de fondo al verde claro */
        transform: scale(0.9); /* Reducción del tamaño al 90% */
    }

    @keyframes fadeIn {
        from {
            opacity: 0;
            transform: translateY(50px);
        }
        to {
            opacity: 1;
            transform: translateY(0);
        }
    }
    
    .overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 9999;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .popup {
        background-color: #d2b48c;
        border-radius: 10px;
        padding: 20px;
        width: 400px;
        text-align: center;
    }
    
    
</style>

<?php
// Estilos para el título principal
$this->registerCss('
    .jumbotron h1 {
        font-family: "Arial Black", sans-serif;
        font-size: 5rem;
        color: #fff;
        text-shadow: 2px 2px 4px #000000;
    }
');

// Estilos para los botones de la sección principal
$this->registerCss('
    .jumbotron .btn {
        background-color: #fff;
        border-color: #fff;
        color: #0074D9;
        font-size: 1.5rem;
        font-weight: bold;
        text-shadow: none;
        transition: all 0.3s ease;
    }

    .jumbotron .btn:hover {
        background-color: #0074D9;
        border-color: #0074D9;
        color: #fff;
        text-shadow: none;
    }
');

// Estilos para las secciones secundarias
$this->registerCss('
    .col-lg-4 h2 {
        font-family: "Helvetica Neue", sans-serif;
        font-size: 2.5rem;
        color: #0074D9;
        text-shadow: 1px 1px 2px #000000;
    }

    .col-lg-4 p {
        font-family: "Helvetica Neue", sans-serif;
        font-size: 1.5rem;
        color: #333;
        line-height: 2rem;
    }

    .col-lg-4 .btn {
        background-color: #0074D9;
        border-color: #0074D9;
        color: #fff;
        font-size: 1.5rem;
        font-weight: bold;
        text-shadow: none;
        transition: all 0.3s ease;
    }

    .col-lg-4 .btn:hover {
        background-color: #fff;
        border-color: #0074D9;
        color: #0074D9;
        text-shadow: none;
    }
');
?>



<div class="site-index">
  
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4" style="margin-top: 130px;">COMPOSISCORES</h1>
        <p style="font-family: 'Arial Black', sans-serif; color: #333; text-shadow: 1px 1px #ccc; text-align: center; margin-top: 50px;" class="lead">Tu Biblioteca Musical</p>
    </div>
    
    <div class="body-content custom-margin-top" style="margin-top: 50px;">
        <div class="row">
            <div class="col-lg-4">
                <h3 style="font-family: 'Arial Black', sans-serif; color: #333; text-shadow: 1px 1px #ccc; text-align: center;">ComposiScores </h3>
                <p style="font-family: Georgia, serif; color: #666; text-align: center;">ComposiScores es una pagina web dedicada a los amantes de la musica que buscan encontrar las partituras de sus composiciones favoritas o informarse sobre algunos de los autores clasicos mas influyentes. Encuentra mas informacion de nuestros compositores en:</p>
                <div style="text-align: center;">
                    <p><a class="btn btn-outline-secondary" href="http://localhost/DAM/aplicaciones/musica/yii2-app-basic/web/index.php/compositores/index" style="transition: all 0.3s ease;">COMPOSITORES &raquo;</a></p>
                </div>
            </div>
            
            <div class="col-lg-4 custom-image-container">
                <img src="https://st.depositphotos.com/1807998/1420/i/950/depositphotos_14204676-stock-photo-music-sheet-page.jpg" alt="vonco.jpg" style="height: 400px; border: 1px solid #ccc; box-shadow: 0 0 10px rgba(0, 0, 0, 0.5); background: linear-gradient(to right, rgba(0, 0, 0, 0.1) 50%, rgba(0, 0, 0, 0.1) 50%);">
            </div>
        </div>
    </div>
            <div id="carouselExample" class="carousel slide" data-ride="carousel" style="margin-top: 70px;">
  <ol class="carousel-indicators">
    <li data-target="#carouselExample" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExample" data-slide-to="1"></li>
    <li data-target="#carouselExample" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <a href="URL_IMAGEN_1" target="_blank">
        <img src="https://st.depositphotos.com/1807998/1420/i/950/depositphotos_14204676-stock-photo-music-sheet-page.jpg" class="d-block w-100" alt="Imagen 1">
      </a>
    </div>
    <div class="carousel-item">
      <a href="URL_IMAGEN_2" target="_blank">
        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6f/Mychtar_and_his_Snowdog.jpg" class="d-block w-100" alt="Imagen 2">
      </a>
    </div>
    <div class="carousel-item">
      <a href="URL_IMAGEN_3" target="_blank">
        <img src="https://pbs.twimg.com/media/CwJtpLrUMAAIc_N.jpg" class="d-block w-100" alt="Imagen 3">
      </a>
    </div>
  </div>
</div>

            
    
<div style="display: flex; justify-content: flex-end;">
    <div style="display: flex; align-items: center; justify-content: center; flex-direction: column; background-color: white; border-radius: 50%; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); padding: 30px; margin-top: 70px;">
        <h4 style="font-family: 'Arial Black', sans-serif; color: #333; text-shadow: 1px 1px #ccc; margin-top: 40px;">¡SABÍAS QUE?</h4>

        <div style="display: flex; justify-content: center; flex-direction: column; align-items: center; margin-top: 10px;">
            <p style="font-size: 20px; font-family: Georgia, serif; color: #333; text-align: center; max-width: 300px; margin: 0 auto;">¿Mozart era un cachondo?</p>
            <a href="#" onclick="openPopup()" style="display: inline-block; background-color: #4CAF50; color: #000; padding: 10px 20px; border-radius: 50%; text-decoration: none; margin-top: 20px; transition: all 0.3s ease;">SABER MÁS</a>
        </div>
    </div>
</div>


    
    <div style="display: flex; align-items: center; justify-content: center; flex-direction: column; background-color: white; border-radius: 50%; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); padding: 50px; margin-top: 70px;">
        <h4 style="font-family: 'Arial Black', sans-serif; color: #333; text-shadow: 1px 1px #ccc; margin-top: 40px;">NUESTRO MÚSICO DE LA SEMANA ES:</h4>
        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6a/Johann_Sebastian_Bach.jpg" alt="vonco.jpg" style="height: 350px; margin-top: 30px;">
        <div style="display: flex; align-items: center; justify-content: center; height: 10vh;">
            <p style="font-family: Georgia, serif; color: #666; font-size: 36px; text-align: center;">JOHAN SEBASTIAN BACH (1685-1750)</p>
        </div>
        <div style="display: flex; justify-content: center; flex-direction: column; align-items: center; margin-top: 10px;">
            <p style="font-size: 20px; font-family: Georgia, serif; color: #333; text-align: center; max-width: 400px; margin: 0 auto;">Johann Sebastian Bach (1685-1750) fue un compositor, organista, clavicembalista y violinista alemán del periodo barroco.</p>
            <a href="http://localhost/Dam/aplicaciones/musica/yii2-app-basic/web/index.php/compositores/view?ID=1" style="display: inline-block; background-color: #4CAF50; color: #000; padding: 10px 20px; border-radius: 50%; text-decoration: none; margin-top: 20px; transition: all 0.3s ease;">SABER MÁS</a>
        </div>
    </div>
</div>


<script>
    var button = document.querySelector('a[href="http://localhost/Dam/aplicaciones/musica/yii2-app-basic/web/index.php/compositores/view?ID=1"]');
    button.addEventListener('mouseover', function() {
        button.classList.add('custom-hover');
    });
    button.addEventListener('mouseout', function() {
        button.classList.remove('custom-hover');
    });
    
     function openPopup() {
        var overlay = document.createElement('div');
        overlay.classList.add('overlay');

        var popup = document.createElement('div');
        popup.classList.add('popup');

        var title = document.createElement('h4');
        title.textContent = 'Título del Pop-up';

        var paragraph = document.createElement('p');
        paragraph.textContent = 'Contenido del Pop-up';

        var closeButton = document.createElement('button');
        closeButton.textContent = 'X';
        closeButton.addEventListener('click', closePopup);

        popup.appendChild(title);
        popup.appendChild(paragraph);
        popup.appendChild(closeButton);

        overlay.appendChild(popup);

        document.body.appendChild(overlay);
    }

    function closePopup() {
        var overlay = document.querySelector('.overlay');
        document.body.removeChild(overlay);
    }
    
    $(document).ready(function() {
    $('#carouselExample').carousel({
      interval: 4000 // Cambia la imagen cada 4 segundos (4000 ms)
    });
  });
</script>
