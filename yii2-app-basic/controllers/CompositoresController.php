<?php

namespace app\controllers;

use app\models\Compositores;
use app\models\db;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CompositoresController implements the CRUD actions for Compositores model.
 */
class CompositoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Compositores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new db();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compositores model.
     * @param int $ID ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($ID),
        ]);
    }

    /**
     * Creates a new Compositores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Compositores();

        $this -> subirfoto($model);
        

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Compositores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $ID ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($ID)
    {
        $model = $this->findModel($ID);

        $this -> subirfoto($model);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Compositores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $ID ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($ID)
    {
        $model =$this->findModel($ID);
        if(file_exists($model->FOTOGRAFIA)){
            
        unlink($model->FOTOGRAFIA);
        }
                
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Compositores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $ID ID
     * @return Compositores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ID)
    {
        if (($model = Compositores::findOne(['ID' => $ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    protected function subirfoto(Compositores $model){
        
            if ($model->load($this->request->post()) ) {
                $model -> FOTO = UploadedFile::getInstance($model, 'FOTO');
                if($model->validate()){
                    if($model -> FOTO){
                        
                        if(file_exists($model->FOTOGRAFIA)){
            
                           unlink($model->FOTOGRAFIA);
                        }
                        
                        $rutaFoto='uploads/'.time()."_".$model->FOTO->baseName.".".$model->FOTO->extension;
                        
                        if($model->FOTO->saveAs($rutaFoto)){
                            $model->FOTOGRAFIA=$rutaFoto;
                            
                        }
                    }
                }
                
                
                
                if($model->save(false)){
                    
                    return $this->redirect(['index']);
                }
                
                
            }
    }
}
