<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compositores".
 *
 * @property int $ID
 * @property string $NOMBRE_COMPLETO
 * @property resource|null $FOTOGRAFIA
 * @property string|null $BIOGRAFIA
 * @property string $FECHA_NACIMIENTO
 * @property string|null $FECHA_MUERTE
 * @property string $INSTRUMENTO
 *
 * @property CompositoresCanciones[] $compositoresCanciones
 */
class Compositores extends \yii\db\ActiveRecord
{
    public $FOTO;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compositores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOMBRE_COMPLETO', 'FECHA_MUERTE','BIOGRAFIA', 'INSTRUMENTO'], 'required'],
            [['BIOGRAFIA'], 'string'],
            [['FOTO'], 'file', 'extensions' => 'jpg,png'],
            [['FECHA_NACIMIENTO', 'FECHA_MUERTE'], 'safe'],
            [['NOMBRE_COMPLETO', 'INSTRUMENTO'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NOMBRE_COMPLETO' => 'Nombre Completo',
            'FOTO' => 'Fotografia',
            'BIOGRAFIA' => 'Biografia',
            'FECHA_NACIMIENTO' => 'Fecha Nacimiento',
            'FECHA_MUERTE' => 'Fecha Muerte',
            'INSTRUMENTO' => 'Instrumento',
        ];
    }

    /**
     * Gets query for [[CompositoresCanciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositoresCanciones()
    {
        return $this->hasMany(CompositoresCanciones::class, ['ID_COMPOSITOR' => 'ID']);
    }
}
